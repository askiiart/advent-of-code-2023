1. Combine the first and last digit on each line to create a single two-digit number.
2. Get the sum of all of those

Input:

```txt
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
```

(12, 38, 15, 77)

Output:

```txt
142
```
