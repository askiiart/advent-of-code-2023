#!/usr/bin/env python3
with open('day-1/input', 'rt') as f:
    numbers = []
    for line in f:
        for char in line:
            if str.isdigit(char):
                digit1 = char
                break

        for i in range(len(line) - 2, -1, -1):
            if str.isdigit(line[i]):
                digit2 = line[i]
                break
        
        numbers.append(int(digit1 + digit2))

total = 0
for num in numbers:
    total += num
print(total)